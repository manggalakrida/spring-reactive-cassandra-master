package com.javatechie.spring.cassandra.api.controller;

import com.javatechie.spring.cassandra.api.model.User;
import com.javatechie.spring.cassandra.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    public Flux<User> getUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/users-age/{age}")
    public Flux<User> getUserByAge(@PathVariable int age) {
        return userRepository.findByAgeLessThan(age);
    }

    @GetMapping("/users-address/{address}")
    public Mono<User> getUser(@PathVariable String address) {
        return userRepository.findByAddress(address);
    }

}
